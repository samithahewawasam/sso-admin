import React from 'react';
import { Admin, Resource } from 'react-admin';
import api from './providers/api';
import auth from './providers/auth'
import AssignRole from './views/role-assign'
import { Route } from 'react-router-dom';
import { UserList } from './views/user';
import { ClientCreate, ClientList } from './views/clients';
import { RoleCreate, RoleList } from './views/roles';
import { PermissionsList } from './views/permissions';
import AssignRolePermissions from './views/role-permissions';

const App = () => (
  <Admin
    dataProvider={api}
    authProvider={auth}
    customRoutes={[
      <Route
        path="/role/assign"
        component={AssignRole}
      />,
      <Route
        path="/role/permissions"
        component={AssignRolePermissions}
      />
    ]}
  >
    <Resource name="users" list={UserList} />
    <Resource name="clients" list={ClientList} create={ClientCreate} />
    <Resource name="roles" list={RoleList} create={RoleCreate} />
    <Resource name="permissions" list={PermissionsList} />
  </Admin>
)

export default App;
