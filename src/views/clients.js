import React from 'react';
import { Create, List,SimpleForm, Toolbar, DeleteButton, Datagrid, TextField, DateField, EmailField, TextInput } from 'react-admin';
import RegisterButton from './buttons/register'
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

const CreateRoleAssignButton = ({ record }) => (
    <Button
        component={Link}
        to={{
            pathname: '/role/assign',
            state: { record: { user_id: record.id } },
        }}
    >
        Assign a role
    </Button>
);

export const ClientList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="name" />
            <EmailField source="email" />
            <DateField source="created_at.date" showTime/>
            <DateField source="updated_at.date" showTime/>
            <CreateRoleAssignButton/>
            <DeleteButton/>
        </Datagrid>
    </List>
);

const ClientCreateToolbar = props => (
    <Toolbar {...props} >
        <RegisterButton/>
    </Toolbar>
);

export const ClientCreate = (props) => (
    <Create {...props}>
        <SimpleForm toolbar={<ClientCreateToolbar/>}>
            <TextInput source="name" />
            <TextInput source="email" />
            <TextInput source="username" />
            <TextInput source="password" />
        </SimpleForm>
    </Create>
);