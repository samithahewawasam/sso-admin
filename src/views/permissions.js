import React from 'react';
import { Create, List,SimpleForm, DeleteButton, Datagrid, TextField, DateField, EmailField, TextInput } from 'react-admin';

export const PermissionsList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="name" />
            <TextField source="description" />
            <DeleteButton/>
        </Datagrid>
    </List>
);

export const PermissionsCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <TextInput source="email" />
            <TextInput source="username" />
            <TextInput source="password" />
        </SimpleForm>
    </Create>
);