import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import { assignRolePermissions as assignRolePermissionsAction } from '../../actions/permissions';

class AssignRolePermissionsButton extends Component {
    handleClick = (data) => {
        const { assignRolePermissions } = this.props;
        assignRolePermissions(data);
    }

    render() {
        const { handleSubmit } = this.props;
        return <Button onClick={handleSubmit(data => this.handleClick(data))}>Assign</Button>;
    }
}

AssignRolePermissionsButton.propTypes = {
    assignRolePermissions: PropTypes.func,
    record: PropTypes.object,
};

export default connect(null, {
    assignRolePermissions: assignRolePermissionsAction,
})(AssignRolePermissionsButton);