import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import { assignRole as assignRoleAction } from '../../actions/role';

class AssignRoleButton extends Component {
    handleClick = (data) => {
        const { assignRole } = this.props;
        assignRole(data);
    }

    render() {
        const { handleSubmit } = this.props;
        return <Button onClick={handleSubmit(data => this.handleClick(data))}>Assign</Button>;
    }
}

AssignRoleButton.propTypes = {
    assignRole: PropTypes.func,
    record: PropTypes.object,
};

export default connect(null, {
    assignRole: assignRoleAction,
})(AssignRoleButton);