import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import { userRegister as userRegisterAction } from '../../actions/register';

class RegisterButton extends Component {
    handleClick = (data) => {
        const { register } = this.props;
        console.log(this.props)
        register(data);
    }

    render() {
        const { handleSubmit } = this.props;
        return <Button onClick={handleSubmit(data => this.handleClick(data))}>Register</Button>;
    }
}

RegisterButton.propTypes = {
    register: PropTypes.func,
    record: PropTypes.object,
};

export default connect(null, {
    register: userRegisterAction,
})(RegisterButton);