import React from 'react';
import { Create, ReferenceInput, SelectInput, List, SimpleForm, Toolbar, CloneButton, Datagrid, TextField, DateField, EmailField, TextInput } from 'react-admin';
import AssignRoleButton from './buttons/assign-role'

const RoleAssignToolbar = props => (
    <Toolbar {...props} >
        <AssignRoleButton />
    </Toolbar>
);

const AssignRole = (props) => (
    <Create {...props} basePath="" resource="">
        <SimpleForm toolbar={<RoleAssignToolbar />}>
            <ReferenceInput label="Role" source="roles_ids" reference="roles">
                <SelectInput optionText="name" />
            </ReferenceInput>
        </SimpleForm>
    </Create>
);

export default AssignRole;