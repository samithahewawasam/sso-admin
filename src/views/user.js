import React from 'react';
import { Create, List,SimpleForm, DeleteButton, Datagrid, TextField, DateField, EmailField, TextInput } from 'react-admin';

export const UserList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="name" />
            <EmailField source="email" />
            <DateField source="created_at.date" showTime/>
            <DateField source="updated_at.date" showTime/>
            <DeleteButton/>
        </Datagrid>
    </List>
);

export const UserCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <TextInput source="email" />
            <TextInput source="username" />
            <TextInput source="password" />
        </SimpleForm>
    </Create>
);