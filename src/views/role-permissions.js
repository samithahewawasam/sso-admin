import React from 'react';
import { Create, ReferenceInput, SelectArrayInput, List, SimpleForm, Toolbar, CloneButton, Datagrid, TextField, DateField, EmailField, TextInput } from 'react-admin';
import AssignRolePermissionsButton from './buttons/assign-role-permissions'

const RolePermissionsToolbar = props => (
    <Toolbar {...props} >
        <AssignRolePermissionsButton />
    </Toolbar>
);

const AssignRolePermissions = (props) => (
    <Create {...props} basePath="" resource="">
        <SimpleForm toolbar={<RolePermissionsToolbar />}>
            <ReferenceInput label="Permissions" source="permissions_ids" reference="permissions">
                <SelectArrayInput optionText="name" />
            </ReferenceInput>
        </SimpleForm>
    </Create>
);

export default AssignRolePermissions;