import React from 'react';
import { Create, List, ArrayField, SingleFieldList, DeleteButton, ChipField,  SimpleForm, Toolbar, Datagrid, TextField, DateField, EmailField, TextInput } from 'react-admin';
import RegisterButton from './buttons/register'
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

const CreateRoleAssignPermissionsButton = ({ record }) => (
    <Button
        component={Link}
        to={{
            pathname: '/role/permissions',
            state: { record: { role_id: record.id } },
        }}
    >
        Assign Permissions
    </Button>
);

export const RoleList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="name" />
            <TextField source="description" />
            <TextField source="display_name" />
            <ArrayField source="permissions.data">
                <SingleFieldList>
                    <ChipField source="name" />
                </SingleFieldList>
            </ArrayField>
            <CreateRoleAssignPermissionsButton/>
            <DeleteButton/>
        </Datagrid>
    </List>
);

const RoleCreateToolbar = props => (
    <Toolbar {...props} >
        <RegisterButton />
    </Toolbar>
);

export const RoleCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <TextInput source="description" />
            <TextInput source="display_name" />
        </SimpleForm>
    </Create>
);

export const AssignRole = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="user_id" />
            <TextInput source="roles_ids"/>
        </SimpleForm>
    </Create>
);