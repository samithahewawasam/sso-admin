export const ASSIGN_ROLE = 'ASSIGN_ROLE';
export const assignRole = (data) => ({
    type: ASSIGN_ROLE,
    payload: { data: { ...data } },
    meta: { resource: 'roles/assign', fetch: 'ASSIGN_ROLE' },
});
