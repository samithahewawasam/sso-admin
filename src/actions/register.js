export const USER_REGISTER = 'USER_REGISTER';
export const userRegister = (data) => ({
    type: USER_REGISTER,
    payload: { data: { ...data } },
    meta: { resource: 'register', fetch: 'REGISTER' },
});
