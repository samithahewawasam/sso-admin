export const ASSIGN_ROLE_PERMISSIONS = 'ASSIGN_ROLE_PERMISSIONS';
export const assignRolePermissions = (data) => ({
    type: ASSIGN_ROLE_PERMISSIONS,
    payload: { data },
    meta: { resource: 'permissions/attach', fetch: 'ASSIGN_ROLE_PERMISSIONS' },
});