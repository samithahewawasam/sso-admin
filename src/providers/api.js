import { stringify, queryString } from 'query-string';
import {
    GET_LIST,
    GET_ONE,
    CREATE,
    UPDATE,
    DELETE,
    UPDATE_MANY,
    DELETE_MANY,
    GET_MANY,
    GET_MANY_REFERENCE,
} from 'react-admin';

const apiUrl = 'http://ec2-3-227-194-181.compute-1.amazonaws.com/api/v1';

const REGISTER = 'REGISTER'
const ASSIGN_ROLE = 'ASSIGN_ROLE'
const ASSIGN_ROLE_PERMISSIONS = 'ASSIGN_ROLE_PERMISSIONS'

/**
 * Maps react-admin queries to my REST API
 *
 * @param {string} type Request type, e.g GET_LIST
 * @param {string} resource Resource name, e.g. "posts"
 * @param {Object} payload Request parameters. Depends on the request type
 * @constant {Object} query Params set for http request
 * @returns {Promise} the Promise for a data response
 */

let query = {}

export default (type, resource, params) => {
    let url = '';
    const access_token = localStorage.getItem('access_token');
    const options = {
        headers: new Headers({
            Accept: 'application/json',
            Authorization: `Bearer ${access_token}`
        }),
    };
    switch (type) {
        case GET_LIST: {
            const { page, perPage } = params.pagination;
            const { field, order } = params.sort;
            const query = {
                sort: JSON.stringify([field, order]),
                range: JSON.stringify([
                    (page - 1) * perPage,
                    page * perPage - 1,
                ]),
                filter: JSON.stringify(params.filter),
            };
            url = `${apiUrl}/${resource}`;
            break;
        }
        case GET_ONE:
            url = `${apiUrl}/${resource}/${params.id}`;
            break;
        case REGISTER:
            url = new URL(`${apiUrl}/${resource}`)
            Object.keys(params.data).forEach(key => url.searchParams.append(key, params.data[key]))
            options.method = 'POST';
            break;
        case CREATE:
            url = new URL(`${apiUrl}/${resource}`)
            Object.keys(params.data).forEach(key => url.searchParams.append(key, params.data[key]))
            options.method = 'POST';
            break;
        case UPDATE:
            url = `${apiUrl}/${resource}/${params.id}`;
            options.method = 'PUT';
            options.body = JSON.stringify(params.data);
            break;
        case ASSIGN_ROLE:
            url = new URL(`${apiUrl}/${resource}`)
            Object.keys(params.data).forEach(key => url.searchParams.append(key, params.data[key]))
            options.method = 'POST';
            break;
        case ASSIGN_ROLE_PERMISSIONS:
            query = {
                'role_id': params.data.role_id,
                'permissions_ids' : params.data.permissions_ids
            }
            url = `${apiUrl}/${resource}?${stringify(query)}`;
            options.method = 'POST';
            break;
        case UPDATE_MANY:
            query = {
                filter: JSON.stringify({ id: params.ids }),
            };
            url = `${apiUrl}/${resource}?${stringify(query)}`;
            options.method = 'PATCH';
            options.body = JSON.stringify(params.data);
            break;
        case DELETE:
            url = `${apiUrl}/${resource}/${params.id}`;
            options.method = 'DELETE';
            break;
        case DELETE_MANY:
            query = {
                filter: JSON.stringify({ id: params.ids }),
            };
            url = `${apiUrl}/${resource}?${stringify(query)}`;
            options.method = 'DELETE';
            break;
        case GET_MANY: {
            query = {
                filter: JSON.stringify({ id: params.ids }),
            };
            url = `${apiUrl}/${resource}?${stringify(query)}`;
            break;
        }
        case GET_MANY_REFERENCE: {
            const { page, perPage } = params.pagination;
            const { field, order } = params.sort;
            query = {
                sort: JSON.stringify([field, order]),
                range: JSON.stringify([
                    (page - 1) * perPage,
                    page * perPage - 1,
                ]),
                filter: JSON.stringify({
                    ...params.filter,
                    [params.target]: params.id,
                }),
            };
            url = `${apiUrl}/${resource}?${stringify(query)}`;
            break;
        }
        default:
            throw new Error(`Unsupported Data Provider request type ${type}`);
    }

    return fetch(url, options)
        .then(res => res.json())
        .then(response => response.hasOwnProperty('data') ? ({
            data: response.data, total: response.data.length
        }) : { data: [], total: 0 });
};
