import { AUTH_LOGIN, AUTH_ERROR } from 'react-admin';
import { showNotification } from 'react-admin';

const client_id = 4
const client_secret = "igulsC60qFL1K6bH1uoBBJwijq8fQZhwlOYMS3Sn"

export default (type, params) => {
    if (type === AUTH_LOGIN) {
        const { username, password } = params;
        const request = new Request('http://ec2-3-227-194-181.compute-1.amazonaws.com/v1/oauth/token', {
            method: 'POST',
            body: JSON.stringify({ username, password, client_id, client_secret, grant_type: "password", scope: "" }),
            headers: new Headers({ 'Content-Type': 'application/json' }),
        })

        return fetch(request)
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then(({ access_token, refresh_token }) => {
                localStorage.setItem('access_token', access_token);
                localStorage.setItem('refresh_token', refresh_token);
            });
    }

    if (type === AUTH_ERROR) {
        
        const status  = params.status;
        if (status === 401 || status === 403) {
            localStorage.removeItem('access_token');
            return Promise.reject();
        }
        return Promise.resolve();
    }

    return Promise.resolve();
}
